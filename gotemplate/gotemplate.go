package gotemplate

import (
  "html/template"
)

/*creates template from string*/
func ParseTemplate(blobs ...string) (t *template.Template, err error) {
  t= template.New("")
  for _, blob := range blobs {
    _, err= t.Parse(blob)
    if err!=nil{
      return
    }
  }
  return
}

func DefaultTemplateParser(files ...string) (t *template.Template) {
  return template.Must(template.ParseFiles(files...))
}
