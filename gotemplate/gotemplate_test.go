package gotemplate

import (
  "fmt"
  "testing"
)
func TestGoTmp(t *testing.T)  {
  files:= []string{
    "sample.html",
  }
  var blob []string
  for _, f := range files {
    b, err:= Asset(f)
    if err!=nil{
      break
    }
    blob = append(blob, string(b))
  }
  fmt.Println(blob)

  tp, err:=ParseTemplate(blob...)
  fmt.Println("declared ", tp.DefinedTemplates())
  fmt.Println(err)
}


func TestGoTmp2(t *testing.T)  {
  files:= []string{
    "sample.html",
  }
  tp := DefaultTemplateParser(files...)
  fmt.Println("declared ", tp.DefinedTemplates())
}
